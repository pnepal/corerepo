using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreApp.Repository;
using Read = Accounts.Read;
using Microsoft.AspNetCore.Mvc;

namespace CoreApp.Controllers
{
    /// <summary>
    /// Accounts
    /// </summary>
    [Produces("application/json")]
    [Route("api/Accounts")]
    public class AccountsController : Controller
    {
        private readonly IAccounts _accounts;
        /// <summary>
        /// DI
        /// </summary>
        /// <param name="accounts"></param>
        public AccountsController(IAccounts accounts)
        {
            _accounts = accounts;
        }

        /// <summary>
        /// Get Accounts
        /// </summary>
        /// <returns>Accounts</returns>
        /// <response code="200">OK</response>
        /// <response code="400">Bad Request</response>
        /// <response code="500">Internal Server Error</response>
        [HttpGet]
        [ProducesResponseType(typeof(List<Read.Accounts>), 200)]
        public async Task<IActionResult> GetAccounts()
        {
            return Ok(await _accounts.GetAccounts());
        }
    }
}