﻿using System;
using System.Collections.Generic;
using Read = Accounts.Read;
using System.Threading.Tasks;

namespace CoreApp.Repository
{
    public interface IAccounts
    {
        Task<IEnumerable<Read.Accounts>> GetAccounts();
    }

    public class Accounts : IAccounts
    {
        public async Task<IEnumerable<Read.Accounts>> GetAccounts()
        {
            return new List<Read.Accounts>
            {
                new Read.Accounts {AccountName = "Account 1"},
                new Read.Accounts {AccountName = "Account 2"}
            };
        }
    }
}
